<?php

return [
    'files' => [
        'max_filesize' => env('IMPORT_FILE_SIZE', 10240),
    ]
];
