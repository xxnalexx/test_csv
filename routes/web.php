<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ImportContactsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::group(['prefix' => 'dashboard'], function() {
        Route::get('/',  function() {
            return Inertia\Inertia::render('Dashboard');
        })->name('dashboard');
    });

    Route::group(['prefix' => 'import-contacts', 'as' => 'import-contacts.'], function() {
        Route::get('/step1',  [ImportContactsController::class, 'uploadFileIndex'])->name('step1');
        Route::post('/step1',  [ImportContactsController::class, 'uploadFileStore']);
        Route::get('/step2',  [ImportContactsController::class, 'mappingFieldsIndex'])->name('step2');
        Route::post('/step2',  [ImportContactsController::class, 'mappingFieldsStore']);
        Route::get('/step3',  [ImportContactsController::class, 'previewMappingIndex'])->name('step3');
        Route::get('/step4',  [ImportContactsController::class, 'selectListIndex'])->name('step4');
        Route::post('/step4',  [ImportContactsController::class, 'runImport']);
        Route::get('/success',  [ImportContactsController::class, 'successIndex'])->name('success');
        Route::get('/cancel',  [ImportContactsController::class, 'cancelImportIndex'])->name('cancel');
    });

});
