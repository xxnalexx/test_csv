<?php

namespace Tests\Feature\ImportContacts;

use App\Models\ImportContactFile;
use App\Models\User;
use App\Services\ImportContactStates\ImportContext;
use App\Services\ImportContactStates\StateMappedFields;
use App\Services\ImportContactStates\StateSelectList;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class RunImportContactTest extends TestCase
{
    use RefreshDatabase;

    private $user;
    private $fileObj;
    private $file;

    public function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub

        $this->user = User::factory()->create();

        \Excel::fake();

        $this->fileObj = ImportContactFile::factory()->create([
            'filename' => 'test.csv'
        ]);
    }


    public function test_success_redirect()
    {
        \Cache::shouldReceive('get')
            ->with('import_contacts_' . $this->user->id)
            ->andReturn([
                'state' => ImportContext::create(),
                'file' => [
                    'id' => $this->fileObj->id
                ]
            ]);

        \Cache::shouldReceive('forget')
            ->once();

        $response = $this->actingAs($this->user)
            ->post(route('import-contacts.step4'), [
                'lists' => [],
            ]);

        $response->assertRedirect(route('import-contacts.success'));
    }

    public function test_list_default_created_state()
    {
        $context = new ImportContext();
        $context->setState(new StateSelectList());
        \Cache::shouldReceive('get')
            ->with('import_contacts_' . $this->user->id)
            ->andReturn([
                'state' => $context,
                'file' => [
                    'id' => $this->fileObj->id
                ]
            ]);

        \Cache::shouldReceive('forget')
            ->once();

        $response = $this->actingAs($this->user)
            ->post(route('import-contacts.step4'), [
                'lists' => [],
            ]);


        $this->assertDatabaseHas('import_contact_lists', [
            'name' => 'test.csv',
        ]);
    }

    public function test_list_existed_was_sync()
    {
        $context = new ImportContext();
        $context->setState(new StateSelectList());
        \Cache::shouldReceive('get')
            ->with('import_contacts_' . $this->user->id)
            ->andReturn([
                'state' => $context,
                'file' => [
                    'id' => $this->fileObj->id
                ]
            ]);

        \Cache::shouldReceive('forget')
            ->once();

        $response = $this->actingAs($this->user)
            ->post(route('import-contacts.step4'), [
                'lists' => [[
                    'id' => 1
                ]],
            ]);

        $this->assertDatabaseMissing('import_contact_lists', [
            'name' => 'test.csv',
        ]);
    }
}
