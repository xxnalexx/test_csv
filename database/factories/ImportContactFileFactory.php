<?php

namespace Database\Factories;

use App\Models\ImportContactFile;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ImportContactFileFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ImportContactFile::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $user = User::factory()->create();
        return [
            'type' => 'text/plain',
            'filename' => $this->faker->shuffleString(10) . $this->faker->fileExtension,
            'user_id' => $user->id,
            'status' => ImportContactFile::STATUS_UPLOADED,
        ];
    }
}
