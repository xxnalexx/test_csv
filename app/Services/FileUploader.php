<?php

namespace App\Services;

use App\Models\ImportContactFile;
use Illuminate\Http\UploadedFile;
use App\Models\User;

class FileUploader
{
    /**
     * @param UploadedFile $file
     * @param User $user
     * @return ImportContactFile
     */
    public function uploadImportContact(UploadedFile $file, User $user): ImportContactFile
    {
        $fileObj = new ImportContactFile();
        $fileObj->type = $file->getMimeType();
        $fileObj->filename = $file->getClientOriginalName();
        $fileObj->user_id = $user->id;
        $fileObj->status = ImportContactFile::STATUS_UPLOADED;

        $file->storeAs(ImportContactFile::UPLOAD_DIR, $fileObj->filename, ['disk' => 'public']);
        $fileObj->save();

        return $fileObj;
    }
}
