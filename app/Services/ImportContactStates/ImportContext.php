<?php


namespace App\Services\ImportContactStates;


class ImportContext
{
    /**
     * @var State $state
     */
    private $state;

    public static function create(): ImportContext
    {
        $import = new self();
        $import->state = new StateUploaded();

        return $import;
    }

    public function setState(State $state)
    {
        $this->state = $state;
    }

    public function proceedToNext()
    {
        $this->state->proceedToNext($this);
    }

    public function toString()
    {
        return $this->state->toString();
    }
}
