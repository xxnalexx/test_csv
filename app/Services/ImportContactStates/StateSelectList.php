<?php


namespace App\Services\ImportContactStates;

use App\Services\ImportContactStates\State;

class StateSelectList implements State
{
    public function proceedToNext(ImportContext $context)
    {
    }

    public static function toString(): string
    {
        return 'select-list';
    }
}
