<?php

namespace App\Services\ImportContactStates;

use App\Services\ImportContactStates\State;

class StateMappedFields implements State
{
    public function proceedToNext(ImportContext $context)
    {
        $context->setState(new StateSelectList());
    }

    public static function toString(): string
    {
        return 'mapped-fields';
    }
}
