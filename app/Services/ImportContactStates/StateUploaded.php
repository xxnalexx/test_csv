<?php


namespace App\Services\ImportContactStates;

use App\Services\ImportContactStates\State;

class StateUploaded implements State
{
    public function proceedToNext(ImportContext $context)
    {
        $context->setState(new StateMappedFields());
    }

    public static function toString(): string
    {
        return 'uploaded';
    }
}
