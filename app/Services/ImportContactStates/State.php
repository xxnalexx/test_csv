<?php

namespace App\Services\ImportContactStates;

interface State
{
    public function proceedToNext(ImportContext $context);

    public static function toString(): string;
}
