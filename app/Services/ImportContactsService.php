<?php

namespace App\Services;

use App\Imports\Contacts\ParseCsvFileFields;
use App\Models\ImportContactFile;
use App\Models\ImportContactList;
use App\Services\ImportContactStates\ImportContext;
use App\Services\ImportContactStates\State;
use App\Services\ImportContactStates\StateSelectList;
use App\Services\ImportContactStates\StateMappedFields;
use App\Services\ImportContactStates\StateUploaded;
use Illuminate\Support\Collection;

class ImportContactsService
{

    /**
     * @param ImportContactFile $importFile
     * @return Collection
     */
    public function getSpreadSheetMeta(ImportContactFile $importFile): array
    {
        $fileData = \Excel::toCollection(new ParseCsvFileFields, $importFile->getFilePath())->first();

        return [
            'rowsCount' => $fileData->count(),
            'fields' => $fileData->first()->toArray(),
        ];
    }

    /**
     * @param array $lists
     * @param string $defaultListName
     * @return Collection
     */
    public function getOrCreateContactLists(array $lists, string $defaultListName)
    {
        $collection = collect([]);

        if (empty($lists)) {
            $newContactList = new ImportContactList;
            $newContactList->name = $defaultListName;
            $newContactList->save();

            $collection->push($newContactList);
        } else {
            $contactListIds = [];

            foreach ($lists as $list) {
                if (isset($list['id'])) {
                    $contactListIds[] = $list['id'];
                } else {
                    $newContactList = new ImportContactList;
                    $newContactList->name = !empty($list['name']) ? $list['name'] : $defaultListName;
                    $newContactList->save();

                    $contactListIds[] = $newContactList->id;
                }
            }

            $collection = ImportContactList::whereIn('id', $contactListIds)->get();
        }

        return $collection;
    }

    /**
     * @param State $context
     * @param $state
     * @return bool
     */
    public function verifyState(?ImportContext $context, $state)
    {
        if (empty($context)) return false;

        if (is_array($state)) {
            return \in_array($context->toString(), $state);
        }

        return $context->toString() === $state;
    }

    public function redirectToState(?ImportContext $context)
    {
        if (empty($context)) return \Redirect::route('import-contacts.step1');

        switch ($context->toString())
        {
            case StateUploaded::toString():
                return \Redirect::route('import-contacts.step2');
            case StateMappedFields::toString():
                return \Redirect::route('import-contacts.step3');
            case StateSelectList::toString():
                return \Redirect::route('import-contacts.step4');
            default: return \Redirect::route('import-contacts.step1');
        }
    }
}
