<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    public static function getImportFields()
    {
        return [
            'name',
            'email',
            'phone'
        ];
    }

    public function lists()
    {
        return $this->belongsToMany('App\Model\ImportContactList', 'contact_lists', 'contact_id', 'list_id');
    }
}
