<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImportContactFile extends Model
{
    use HasFactory;

    public const UPLOAD_DIR = 'import-contacts';

    public const STATUS_UPLOADED = 'uploaded';

    public function getFilePath()
    {
        return \Storage::disk('public')->path(self::UPLOAD_DIR . DIRECTORY_SEPARATOR . $this->filename);
    }
}
