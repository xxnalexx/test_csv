<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImportContactList extends Model
{
    use HasFactory;

    public function contacts()
    {
        return $this->belongsToMany('App\Model\Contact', 'contact_lists', 'list_id', 'contact_id');
    }
}
