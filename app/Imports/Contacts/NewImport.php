<?php

namespace App\Imports\Contacts;

use App\Models\Contact;
use App\Models\CustomAttribute;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class NewImport implements ToCollection, ShouldQueue, WithChunkReading
{
    private $lists = [];

    private $mappedFields = [];

    public function __construct(array $params)
    {
        $this->lists = !empty($params['lists']) ? $params['lists'] : [];
        $this->mappedFields = !empty($params['mappedFields']) ? $params['mappedFields'] : [];
    }

    /**
     * @param Collection $rows
     */
    public function collection(Collection $rows)
    {
        $contactColumns = Contact::getImportFields();
        $syncContactLists = [];

        foreach ($rows as $keyRow => $row) {
            $contact = new Contact;
            $contact->team_id = 0;
            $contact->sticky_phone_number_id = 0;
            $customFields = [];

            for ($i = 0; $i < count($row); $i++) {

                if (empty($this->mappedFields[$i])) continue;

                if (\in_array($this->mappedFields[$i], $contactColumns)) {
                    $contact[$this->mappedFields[$i]] = $row[$i];
                } else {
                    $customFields[$keyRow] = [
                      'key' => $this->mappedFields[$i],
                      'value' => $row[$i]
                    ];
                }
            }

            $contact->save();

            if (!empty($customFields)) {
                foreach ($customFields as &$customField) {
                    $customField['contact_id'] = $contact->id;
                }

                CustomAttribute::insert($customFields);
            }

            foreach ($this->lists as $list) {
                $syncContactLists[] = [
                    'contact_id' => $contact->id,
                    'list_id' => $list['id'],
                ];
            }
        }

        if (!empty($syncContactLists)) {
            \DB::table('contact_lists')->insert($syncContactLists);
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
