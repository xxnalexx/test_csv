<?php

namespace App\Imports\Contacts;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;

class ParseCsvFileFields implements ToCollection
{

    /**
     * @param array $row
     *
     * @return null
     */
    public function collection(Collection $rows)
    {
    }

}
