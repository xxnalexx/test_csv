<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImportContacts\MappingColumnsRequest;
use App\Http\Requests\ImportContacts\RunImportRequest;
use App\Http\Requests\ImportContacts\UploadFileRequest;
use App\Http\Resources\ImportContacts\ImportListCollection;
use App\Imports\Contacts\NewImport;
use App\Imports\Contacts\ParseCsvFileFields;
use App\Models\Contact;
use App\Models\ImportContactFile;
use App\Models\ImportContactList;
use App\Services\FileUploader;
use App\Services\ImportContactsService;
use App\Services\ImportContactStates\ImportContext;
use App\Services\ImportContactStates\StateMappedFields;
use App\Services\ImportContactStates\StateSelectList;
use App\Services\ImportContactStates\StateUploaded;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ImportContactsController extends Controller
{
    private $importContactsService;

    public function __construct(ImportContactsService $importContactsService)
    {
        $this->importContactsService = $importContactsService;
    }

    public function uploadFileIndex()
    {
        return Inertia::render('ImportContacts/Step1');
    }

    public function uploadFileStore(UploadFileRequest $request, FileUploader $fileUploader)
    {
        $stepsData = $this->getStepsData();

        $importFile = $fileUploader->uploadImportContact($request->file, \Auth::user());
        $fileMeta = $this->importContactsService->getSpreadSheetMeta($importFile);

        $stepsData['file'] = array_merge($importFile->toArray(), $fileMeta);
        $stepsData['state'] = ImportContext::create();
        $this->setStepsData($stepsData);

        return \Redirect::route('import-contacts.step2');
    }

    public function mappingFieldsIndex()
    {
        $stepsData = $this->getStepsData();

        if (!$this->importContactsService->verifyState($stepsData['state'], StateUploaded::toString())) {
            return $this->importContactsService->redirectToState($stepsData['state']);
        }

        return Inertia::render('ImportContacts/Step2', [
            'file' => $stepsData['file'],
            'contactColumns' => Contact::getImportFields()
        ]);
    }

    public function mappingFieldsStore(MappingColumnsRequest $request)
    {
        $stepsData = $this->getStepsData();
        $stepsData['mappedFields'] = $request->columns;

        $context = $stepsData['state'];
        $context->proceedToNext();
        $stepsData['state'] = $context;

        $this->setStepsData($stepsData);

        return \Redirect::route('import-contacts.step3');
    }

    public function previewMappingIndex()
    {
        $stepsData = $this->getStepsData();

        if (!$this->importContactsService->verifyState($stepsData['state'], StateMappedFields::toString())) {
            return $this->importContactsService->redirectToState($stepsData['state']);
        }

        return Inertia::render('ImportContacts/Step3', [
            'mappedFields' => $stepsData['mappedFields'],
            'spreadSheetFields' => $stepsData['file']['fields'],
        ]);
    }

    public function selectListIndex()
    {
        $stepsData = $this->getStepsData();

        if (!$this->importContactsService->verifyState($stepsData['state'], [StateMappedFields::toString(), StateSelectList::toString()])) {
            return $this->importContactsService->redirectToState($stepsData['state']);
        }

        $context = $stepsData['state'];
        $context->proceedToNext();
        $stepsData['state'] = $context;

        $this->setStepsData($stepsData);

        $importLists = ImportContactList::all();

        return Inertia::render('ImportContacts/Step4', [
            'importLists' => new ImportListCollection($importLists)
        ]);
    }

    public function runImport(RunImportRequest $request)
    {
        $stepsData = $this->getStepsData();
        /**
         * @var ImportContactFile $importContactFile
         */
        $importContactFile = ImportContactFile::find($stepsData['file']['id']);
        $contactLists = $this->importContactsService->getOrCreateContactLists($request->lists, $importContactFile->filename);
        $stepsData['lists'] = $contactLists->toArray();

        $this->clearStepsData();

        \Excel::queueImport(new NewImport($stepsData), $importContactFile->getFilePath());

        return \Redirect::route('import-contacts.success');
    }

    public function successIndex()
    {
        return Inertia::render('ImportContacts/Success');
    }

    public function cancelImportIndex()
    {
        $this->clearStepsData();

        return \Redirect::route('dashboard');
    }

    private function getStepsData(): ?array
    {
        return \Cache::get('import_contacts_' . \Auth::user()->id);
    }

    private function setStepsData(array $data): void
    {
        \Cache::put('import_contacts_' . \Auth::user()->id, $data, now()->addDay());
    }

    private function clearStepsData(): void
    {
        \Cache::forget('import_contacts_' . \Auth::user()->id);
    }
}
