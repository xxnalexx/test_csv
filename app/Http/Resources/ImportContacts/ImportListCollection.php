<?php

namespace App\Http\Resources\ImportContacts;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ImportListCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function ($item) {
                return new ImportListResource($item);
            }),
        ];
    }
}
