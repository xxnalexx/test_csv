<?php

namespace App\Http\Requests\ImportContacts;

use Illuminate\Foundation\Http\FormRequest;

class UploadFileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $maxFileSize = config('import_contacts.files.max_filesize');

        return [
            'file' => [
                'required',
                'file',
                'mimes:csv,txt',
                'max:' .  $maxFileSize
            ]
        ];
    }
}
