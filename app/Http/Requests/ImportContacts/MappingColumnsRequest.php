<?php

namespace App\Http\Requests\ImportContacts;

use App\Models\Contact;
use App\Rules\FieldsExistsRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MappingColumnsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'columns' => [
                'required',
                'array',
                new FieldsExistsRule(Contact::getImportFields())
            ]
        ];
    }
}
